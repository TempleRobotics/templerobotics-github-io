import { EventSignUpProps } from '../pages/events/EventSignUpStyles'

const EVENT_SIGNUP_INFO: EventSignUpProps[] = [
	{
		title: 'Robotics Sign Up',
		description: 'Sign up for the club!',
		src: 'https://docs.google.com/forms/d/e/1FAIpQLSea1tnIeueKhYVQjzDB4B3Eu9SW2346thjVDTSs5X0GFM-5vQ/viewform?usp=sf_link'
	}
	// {
	// 	title: 'Build Day Sign Up',
	// 	description: 'Sign up for the outreach build days!',
	// 	src: 'https://docs.google.com/forms/d/e/1FAIpQLSdOjuVGK6zYMWIRCSSR3LGqVTssxk2A5HxDehjSpYo6C3tGAw/viewform?usp=sf_link'
	// }
]

export default EVENT_SIGNUP_INFO
