import styled from 'styled-components'

import { COLORS } from '../../../tools/Constants'

export default class CopyrightStyles {
	static readonly CopyrightContainer = styled.div`
        background-color: ${COLORS.PRIMARY};
        text-align: center;
        height: 100%;
	`
}
